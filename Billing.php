<?php
 if (!defined('BASEPATH')) exit('No direct script access allowed');
  class Billing {
     
     /*
     * Status field values
     * Always one of the following.
     */ 
     const BAD_DATA = "NotSoReally-baddata";
     const DECLINE = "decline";   
     const APPROVED = "approved";
     const ACCEPTED = "accepted";               // Accepted into System
     const ERROR    = "error";

     /*
     * Decline type field values
     */ 
     const ADDRESS_VERIFCATION = "avs";

     /*
     * Address Verify Code field
     */
     const STREET_AND_ZIP_NOT_MATCHING = "N";   //Street address and postal code do not match.

     /*
     * CVV Response Codes
     */
     const CVV_MATCH            = "M";      // CVV2/CVC2/Discover CID Match
     const CVV_NNOT_MATCH       = "N";      // CVV2/CVC2/Discover CID No Match
     const CVV_NOT_PROCEESED    = "P";      // Not Processed.This data was not forwarded to the issuer
     const CVV_NOT_SENT         = "S" ;     // SAcknowledgement that CVV2/CVC2/CID  was not sent.
     // This would happen because the “cvvstatus�? was set to  "notpresent�?
     const CVV_NOT_SUPPORTTED   = "U";      // Issuer doesn’t support this field.            


     private $_transaction_status;
     private $_transaction_id;
     private $_authorization_code;
     private $_ccv_response_code;
     private $_avs_response_code;
     private $_decline_type;
     private $_error_type;
     private $_offending_fields;
     private $_billing_id;
     private $_tclink; 

     function __construct()
     {
         if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
             $this->_is_windows  = true;
         } else {
             $this->_is_windows  = false;
         }
     }  
     
     
     
     /*************************
     * function create_billing_account ($cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address, $cc_zip,$amount).
     * 
     * @param mixed $cc_number
     * @param mixed $cc_exp
     * @param mixed $cc_cvv
     * @param mixed $cc_name
     * @param mixed $cc_address
     * @param mixed $cc_zip
     * @param mixed $amount
     */
     public function create_billing_account($cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address, $cc_zip,$amount,$start_date=null,$auth_now='y',$cycle='1m')
     {
         
         if ( $start_date == NULL )
         {
             $start_date = date('Y-m-d', time());
         }

         //
         // strip the decimal in the amount for TC
         $amount = str_replace(".","",$amount);

         if ( $this->_is_windows == true)
             $this->create_billing_account_win($cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address, $cc_zip,$amount,$start_date,$auth_now,$cycle);
         else
             $this->create_billing_account_nix($cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address, $cc_zip,$amount,$start_date,$cycle);

     }
     private function create_billing_account_win($cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address, $cc_zip,$amount,$start_date=null,$auth_now,$cycle)
     {
         
         $this->_tclink = new COM("TCLinkCOM.TClink");
         $this->_tclink->PushNameValue("custid=". $this->_id1);
         $this->_tclink->PushNameValue("password=". $this->_id2);
         $this->_tclink->PushNameValue("action=store");
         $this->_tclink->PushNameValue("amount=".$amount);
         $this->_tclink->PushNameValue("cc=".$cc_number);
         $this->_tclink->PushNameValue("exp=".$cc_exp);  
         $this->_tclink->PushNameValue("cvv=".$cc_cvv);    
         $this->_tclink->PushNameValue("name=".$cc_name); 
         $this->_tclink->PushNameValue("address1=".$cc_address); 
         $this->_tclink->PushNameValue("zip=".$cc_zip); 
         $this->_tclink->PushNameValue("authnow=" . $auth_now);
         $this->_tclink->PushNameValue("avs=y");
         $this->_tclink->PushNameValue("cycle=".$cycle);
         $this->_tclink->PushNameValue("start=". $start_date);  // yyyy-mm-dd 

         $this->submit_request_win();

     }  
     private function create_billing_account_nix($cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address, $cc_zip,$amount,$start_date=null,$auth_now)
     {

         if ( $start_date == NULL )
         {
             $start_date = date('Y-m-d', time());
         }
         $params["custid"]  =      $this->_id1;
         $params["password"]=      $this->_id2;
         $params["action"]  =      "store";
         $params["amount"]  =      $amount;
         $params["cc"]      =      $cc_number;
         $params["exp"]     =     $cc_exp;
         $params["cvv"]     =     $cc_cvv;     
         $params["name"]    =     $cc_name;
         $params["address1"]=     $cc_address;
         $params["zip"]     =     $cc_zip;
         $params["authnow"] =     $auth_now;
         $params["avs"]     =     "y";
         $params["cycle"]   =   $cycle;
         $params["start"]   =   $start_date;  // yyyy-mm-dd         

         $this->submit_request_nix($params);
;
     }  


    
    
    
    
    /*********************************
    * update_billing_account ($billingid, $amount)
    * 
    * @param mixed $billingid
    * @param mixed $amount
    */
    public function update_billing_account($billingid, $amount)
    {
                 //
         // strip the decimal in the amount for TC
         $amount = str_replace(".","",$amount);
         
         
        if ( $this->_is_windows == true)
            $this->update_billing_account_win($billingid, $amount);
        else
            $this->update_billing_account_nix($billingid, $amount);        
    }
    private function update_billing_account_win($billingid, $amount)
    {
        $this->_tclink = new COM("TCLinkCOM.TClink");
        $this->_tclink->PushNameValue("custid=". $this->_id1);
        $this->_tclink->PushNameValue("password=". $this->_id2);
        $this->_tclink->PushNameValue("action=store");
        $this->_tclink->PushNameValue("billingid=".$billingid);
        $this->_tclink->PushNameValue("amount=".$amount);

        $this->submit_request_win();

    }    
    private function update_billing_account_nix($billingid, $amount)
    {
     
        $params["custid="]      = $this->_id1;
        $params["password="]    = $this->_id2;
        $params["action=store"];
        $params["billingid="]  = $billingid;
        $params["amount="]     = $amount;

        $this->submit_request_nix($params);

    } 
    
    
    
    
    /**********************************
    * function update_billing_information ($billingid, $cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address,$cc_zip)
    * 
    * @param mixed $billingid
    * @param mixed $cc_number
    * @param mixed $cc_exp
    * @param mixed $cc_cvv
    * @param mixed $cc_name
    * @param mixed $cc_address
    * @param mixed $cc_zip
    */
    public function update_billing_information($billingid, $cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address,$cc_zip)
    {
        if ( $this->_is_windows == true)
            $this->update_billing_information_win($billingid, $cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address,$cc_zip);
        else
            $this->update_billing_information_nix($billingid, $cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address,$cc_zip);        
    }
    private function update_billing_information_nix($billingid,$cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address,$cc_zip)
    {
        $params["custid"]  =      $this->_id1;
        $params["password"]=      $this->_id2;
        $params["action"]  =      "store";
        $params["billingid"]  =   $billingid;
        $params["cc"]      =      $cc_number;
        $params["exp"]     =     $cc_exp;
        $params["cvv"]     =     $cc_cvv;     
        $params["name"]    =     $cc_name;
        $params["address1"]=     $cc_address;
        $params["zip"]     =     $cc_zip;   
        $this->submit_request_nix($params);
      
        // Send the hash to TrustCommerce for processing
       // $result = tclink_send($params);
    }   
    private function update_billing_information_win($billingid, $cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address,$cc_zip)
    {
        $this->_tclink = new COM("TCLinkCOM.TClink");
        $this->_tclink->PushNameValue("custid=". $this->_id1);
        $this->_tclink->PushNameValue("password=". $this->_id2);
        $this->_tclink->PushNameValue("action=store");
        $this->_tclink->PushNameValue("billingid=".$billingid);
        $this->_tclink->PushNameValue("cc=".$cc_number);
        $this->_tclink->PushNameValue("exp=".$cc_exp);  
        $this->_tclink->PushNameValue("cvv=".$cc_cvv);    
        $this->_tclink->PushNameValue("name=".$cc_name); 
        $this->_tclink->PushNameValue("address1=".$cc_address); 
        $this->_tclink->PushNameValue("zip=".$cc_zip);        
        $this->submit_request_win();
        
    }   
      
      
    
    
    
    
    /***********************
    * verify_billing_information( $cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address,$cc_zip)
    *   
    * @param mixed $cc_number
    * @param mixed $cc_exp
    * @param mixed $cc_cvv
    * @param mixed $cc_name
    * @param mixed $cc_address
    * @param mixed $cc_zip
    */
    public function verify_billing_information( $cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address,$cc_zip)
    {
        if ( $this->_is_windows == true)
            $this->verify_billing_information_win( $cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address,$cc_zip);
        else
            $this->verify_billing_information_nix( $cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address);        
    }
    private function verify_billing_information_nix($cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address,$cc_zip)
    {
        $params["custid"]  =      $this->_id1;
        $params["password"]=      $this->_id2;
        $params["action"]  =      "verify";
        $params["cc"]      =      $cc_number;
        $params["exp"]     =     $cc_exp;
        $params["cvv"]     =     $cc_cvv;     
        $params["name"]    =     $cc_name;
        $params["address1"]=     $cc_address;
        $params["zip"]     =     $cc_zip; 
        $params["avs"]     =     "y";    
        $this->submit_request_nix($params);

    }       
    private function verify_billing_information_win( $cc_number, $cc_exp, $cc_cvv, $cc_name, $cc_address,$cc_zip)
    {
        $this->_tclink = new COM("TCLinkCOM.TClink");
        $this->_tclink->PushNameValue("custid=". $this->_id1);
        $this->_tclink->PushNameValue("password=". $this->_id2);
        $this->_tclink->PushNameValue("action=verify");
        $this->_tclink->PushNameValue("cc=".$cc_number);
        $this->_tclink->PushNameValue("exp=".$cc_exp);  
        $this->_tclink->PushNameValue("cvv=".$cc_cvv);    
        $this->_tclink->PushNameValue("name=".$cc_name); 
        $this->_tclink->PushNameValue("address1=".$cc_address); 
        $this->_tclink->PushNameValue("zip=".$cc_zip);   
        $this->_tclink->PushNameValue("avs="."y");     
        $this->submit_request_win();
        
    }   
      
    
    
     /*********************************
    * unstore_billingid ($billingid)
    * 
    * @param mixed $billingid
    * 
    */
    public function unstore_billingid ($billingid)
    {
        if ( $this->_is_windows == true)
            $this->unstore_billingid_win($billingid);
        else
            $this->unstore_billingid_nix($billingid);        
    }    
    private function unstore_billingid_win($billingid)
    {
        $this->_tclink = new COM("TCLinkCOM.TClink");
        $this->_tclink->PushNameValue("custid=". $this->_id1);
        $this->_tclink->PushNameValue("password=". $this->_id2);
        $this->_tclink->PushNameValue("action=unstore");
        $this->_tclink->PushNameValue("billingid=".$billingid);
 

        $this->submit_request_win();

    }      
    private function unstore_billingid_nix($billingid)
    {
     
        $params["custid="]      = $this->_id1;
        $params["password="]    = $this->_id2;
        $params["action="]       =  "unstore";
        $params["billingid="]  = $billingid;


        $this->submit_request_nix($params);

    } 
    
    
    
    
    public function charge_once()
    {
            //sale
    }
    
    
    
    public function get_results()
    {
        $response = array();
        $response["transaction_status"] =  $this->_transaction_status;
        $response["transaction_id"]     =  $this->_transaction_id;
        $response["authorization_code"] =  $this->_authorization_code;
        $response["error_type"]         =  $this->_errortype;
        $response["decline_type"]       =  $this->_declinetype;
        $response["cvv_response_code"]  =  $this->_cvv_response_code;
        $response["avs_response_code"]  =  $this->_avs_response_code;
        $response["offending_fields"]  =  $this->_offending_fields;
        $response["billingid"]          =  $this->_billingid;
        
        return $response;
    }
    
     public function get_billingId()
    {
            return $this->_billingid;
    }
    
    //
    // WIndows
    //
    private function submit_request_win()
    {
        $this->_tclink->Submit();
                                                
        $this->_transaction_status   = $this->_tclink->GetResponse("status");
    
        $this->_transaction_id       = $this->_tclink->GetResponse("transid");
        $this->_authorization_code   = $this->_tclink->GetResponse("authcode");
        $this->_errortype            = $this->_tclink->GetResponse("errortype");
        $this->_declinetype          = $this->_tclink->GetResponse("declinetype");
        $this->_cvv_response_code    = $this->_tclink->GetResponse("cvv");
        $this->_avs_response_code    = $this->_tclink->GetResponse("avs");
        $this->_offending_fields    = $this->_tclink->GetResponse("offenders");

        $this->_billingid             = $this->_tclink->GetResponse("billingid");
        
    }  
    //
    // *NIX
    //
    private function submit_request_nix($params)
    {

        $result = tclink_send($params);


        if ( array_key_exists('status',$result) == true )
            $this->_transaction_status   = $result["status"];
        else
            $this->_transaction_status   = null;

        if ( array_key_exists('transid',$result) == true )
            $this->_transaction_id       = $result["transid"];
        else
            $this->_transaction_id       = null;

        if ( array_key_exists('authcode',$result) == true )
            $this->_authorization_code   = $result["authcode"];
        else
            $this->_authorization_code   = null;

        if ( array_key_exists('errortype',$result) == true )
            $this->_errortype            = $result["errortype"];
        else
            $this->_errortype            = null;

        if ( array_key_exists('declinetype',$result) == true )
            $this->_declinetype          = $result["declinetype"];
        else
            $this->_declinetype          =null; 


        if ( array_key_exists('cvv',$result) == true )
            $this->_cvv_response_code    = $result["cvv"];
        else
            $this->_cvv_response_code    = null;

        if ( array_key_exists('avs',$result) == true )
            $this->_avs_response_code    = $result["avs"];
        else
            $this->_avs_response_code    = null;

        if ( array_key_exists('billingid',$result) == true )
            $this->_billingid             = $result["billingid"];
        else
            $this->_billingid             = null;
            
        if ( array_key_exists('offenders',$result) == true )
            $this->_offending_fields             = $result["offenders"];
        else
            $this->_offending_fields              = null;
            

    } // end
    
    
    /******************
    * function error_occured()
    * 
    */
    public function error_occurred()
    {

        if  ( $this->_transaction_status == Billing::BAD_DATA  ||
        $this->_transaction_status == Billing::DECLINE || 
        $this->_transaction_status  == Billing::ERROR  
        )
        {
            return true;
        }

        return false;
    } // end  error_occurred()



    /******************
    * get_transaction_status().
    * 
    */
    public function get_transaction_status()
    {

        return  $this->_transaction_status;
    } 


    /*************************************************
    * Non TC billing functions
    *************************************************/
    
    /*******************
    * get_cc_type( $cc_nummber ).
    * 
    * @param mixed $cc_nummber
    * @return mixed
    */
    public function get_cc_type( $cc_nummber )
    {


        /*
        * mastercard: Must have a prefix of 51 to 55, and must be 16 digits in length.
        * Visa: Must have a prefix of 4, and must be either 13 or 16 digits in length.
        * American Express: Must have a prefix of 34 or 37, and must be 15 digits in length.
        * Diners Club: Must have a prefix of 300 to 305, 36, or 38, and must be 14 digits in length.
        * Discover: Must have a prefix of 6011, and must be 16 digits in length.
        * JCB: Must have a prefix of 3, 1800, or 2131, and must be either 15 or 16 digits in length.
        */

        if (preg_match("/^5[1-5][0-9]{14}$/", $cc_nummber))
            return "Mastercard";

        if (preg_match("/^4[0-9]{12}([0-9]{3})?$/", $cc_nummber))
            return "Visa";

        if (preg_match("/^3[47][0-9]{13}$/", $cc_nummber))
            return "American Express";

        if (preg_match("/^3(0[0-5]|[68][0-9])[0-9]{11}$/",$cc_nummber))
            return "Diners Club";

        if (preg_match("/^6011[0-9]{12}$/", $cc_nummber))
            return "Discover";

        if (preg_match("/^(3[0-9]{4}|2131|1800)[0-9]{11}$/", $cc_nummber))
            return "JCB";

    }


    /*********************
    * calculate_nextpayment_date($date,$format=null)
    * 
    * @param mixed $date
    * @param mixed $format
    */
    public function  calculate_nextpayment_date($date,$format=null)
    {
        $start = new DateTime($date, new DateTimeZone("America/New_York"));
        $end = clone $start;
        $end->modify('+1 month');
        
        if (($start->format('m')+1) == 12 )
            $x=($start->format('m')+1);
        else
            $x = ($start->format('m')+1)%12;

        while ($x != $end->format('m')) {

            $end->modify('-1 day');
        }

        if ( $format != null )
            return $end->format($format);
        else
            return $end->format('Y-m-d');
            
    } //emd function calculate_nextpayment_date()
     
  }
?>
